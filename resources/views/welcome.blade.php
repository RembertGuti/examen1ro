<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Examen</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        

        <!--IMAGEN EN LA PARTE SUPERIOR CENTRAL
        <center>
        <img src="//campus.lapaz.emi.edu.bo/pluginfile.php/1/theme_essential/logo/1559047772/logo-emi.png" title="Logo de la EMI" class="img-responsive" alt="Página Principal">
        </center>

        -->

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
                
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
               
            }
            

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 0px;
                top: 8px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel Gutierrez
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
      
            
        
            
        </div>

     
            <!-- PARA TABLA -->
            <center>
        <img src="//campus.lapaz.emi.edu.bo/pluginfile.php/1/theme_essential/logo/1559047772/logo-emi.png" title="Logo de la EMI" class="img-responsive" alt="Página Principal">
        </center>
        <center>
        <table class="egt">

        <caption> <h1>ESCUELA MILITAR DE INGENIERIA "ENTRENAMIENTO" </h1>  </caption>

        <thead>

        <tr>

        <th>LUNES</th>

        <th>MIERCOLES</th>

        <th>VIERNES</th>


        </tr>

        </thead>
        
        <tbody style="background: rgba(128, 255, 0, 0.3); border: 1px solid rgba(100, 200, 0, 0.3);">

        <tr>

        <td><center> <h4> STEP </h4></center> </td>

        <td><center> <h4> AERO - LOCAL </h4></center> </td>

        <td><center> <h4> GIM.LOCALIZADA </h4></center> </td>

        </tr>

        </tbody>

        <tbody style="background: rgba(300, 200, 0, 0.3); border: 10px solid rgba(300, 200, 0, 0.3);">
           
        <td> <center> <h4> MUSCULACION </h4> </center> </td>
 
        <td><center> <h4> MUSCULACION </h4></center> </td>

        <td> <center><h4> MUSCULACION </h4></center> </td>
           
        </tr>

        <tbody style="background: rgba(128, 255, 0, 0.3); border: 1px solid rgba(100, 200, 0, 0.3);">

        <tr>

        <td> <h4> GIMNASIA LOCALIZADA </h4> </td>

        <td> <h4> GIMNASIA LOCALIZADA </h4> </td>

        <td> <h4> GIMNASIA LOCALIZADA </h4> </td>

        </tr>

        </tbody>

        </tbody>


        </table>

      
        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> <h4>ANTERIOR</h4></font></font></button>
        <button type="submit" class="btn btn-success btn-flat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><h4>SIGUIENTE</h4></font></font></button>

    </body>
</html>
